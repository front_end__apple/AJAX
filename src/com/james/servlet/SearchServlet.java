package com.james.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

/**
 * Servlet implementation class SearchServlet
 */
@WebServlet("/SearchServlet")
public class SearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static List<String> datas = new ArrayList<>();
	
	static {
		datas.add("java");
		datas.add("java1803");
		datas.add("javaee");
		datas.add("javase");
		datas.add("jsp");
		datas.add("jstl");
		datas.add("xml");
		datas.add("javascript");
		datas.add("css");
	}
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String keyword = request.getParameter("keyword");
		
		List<String> result = getContent(keyword);
		
		String json = new Gson().toJson(result);
		
		response.getWriter().write(json);
	}
	
	private List<String> getContent(String keyword){
		List<String> list = new ArrayList<>();
		
		if(keyword == null || keyword.isEmpty()) {
			return list;
		}
		
		for (String data : datas) {
			if(data.contains(keyword)) {
				list.add(data);
			}
		}
		
		Collections.sort(list);
		
		return list;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
